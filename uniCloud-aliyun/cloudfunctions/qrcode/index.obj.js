module.exports = {
	_before: function() { // 通用预处理器

	},
	/**
	 * method1方法描述
	 * @param {string} param1 参数1描述
	 * @returns {object} 返回值描述
	 */

	async getQrcode(qrCodeOptions) {
		let result = null
		const res = await uniCloud.httpclient.request('https://api.weixin.qq.com/cgi-bin/token', {
			method: "GET",
			data: {
				grant_type: "client_credential",
				appid: "your appId",
				secret: "your appSecret"
			},
			dataAsQueryString: true,
			contentType: "json",
			dataType: "json"
		});
		console.log(".qq.com/cgi-bin/token", res)
		if(res && res.data && res.data.access_token){
			const res1 = await uniCloud.httpclient.request(
				'https://api.weixin.qq.com/wxa/getwxacode?access_token=' + res.data.access_token, {
					method: "POST",
					data: qrCodeOptions,
					contentType: "json",
					dataType: "arraybuffer"
				});
			console.log("ixin.qq.com/wxa/getwxacode", res1)
			if(res1 && res1.data){
				const options = {
					cloudPath: Date.now() + 'Qrcode.jpg',
					fileContent: res1.data
				}
				console.log("上传文件的Options", options)
				result = uniCloud.uploadFile(options)
				console.log("上传小程序码",result)
			}else{
				result = res1
			}
			
		}else{
			result = res
		}
		
		// 返回结果
		return result
	}
}
