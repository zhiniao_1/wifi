// 插屏广告
var interstitialAd = null;
let interstitial = {
	//id就是传入的广告位id
	load(id) {
		if (uni.createInterstitialAd) {
			interstitialAd = uni.createInterstitialAd({
				adUnitId: id
			})
			interstitialAd.onLoad(() => {
				console.log('插屏 广告加载成功')
			})
			interstitialAd.onError((err) => {
				console.log('插屏 广告加载失败', err)
			})
			interstitialAd.onClose((res) => {
				console.log('插屏 广告关闭', res)
			})
		}
	},
	show() {
		if (interstitialAd) {
			interstitialAd.show().catch((err) => {
				console.error("插屏显示广告失败", err)
			})
		}
	}
}

// 激励视频广告
let videoAd = null;
let rewarded = {
	load(id, callBack) {
		if (uni.createRewardedVideoAd) {
			videoAd = uni.createRewardedVideoAd({
				adUnitId: id
			})
			videoAd.onError(err => {})
			videoAd.onClose((e) => {
				const status = e.detail
				console.log("激励视频-用户点击了【关闭广告】按钮")
				if (status && status.isEnded) {
					console.log("激励视频-正常播放结束 " + status.isEnded);
					callBack()
				} else {
					console.log("激励视频-播放中途退出 " + status.isEnded);
				}
			})
		}
	},
	show() {
		if (videoAd) {
			videoAd.show().catch(() => {
				// 失败重试
				videoAd.load()
					.then(() => videoAd.show())
					.catch(err => {
						console.log('激励视频 广告显示失败')
					})
			})
		}
	}​
}

export {
	interstitial,
	rewarded
}
